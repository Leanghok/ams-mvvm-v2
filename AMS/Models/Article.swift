//
//  Article.swift
//  AMS
//
//  Created by Hour Leanghok on 12/17/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import Foundation

struct JSONArticleResponse: Codable{
    var code : String
    var message: String
    var articles: [Article]?
    
    private enum CodingKeys: String, CodingKey{
        case code = "CODE"
        case message = "MESSAGE"
        case articles = "DATA"
    }
}

struct Article: Codable{
    var id: Int
    var title: String?
    var imageUrl: String?
    var description: String?
    private enum CodingKeys: String, CodingKey{
        case id = "ID"
        case title = "TITLE"
        case imageUrl = "IMAGE"
        case description = "DESCRIPTION"
    }
}

//
//  DeleteViewModel.swift
//  AMS
//
//  Created by Hour Leanghok on 12/21/18.
//  Copyright © 2018 Hour Leanghok. All rights reserved.
//

import Foundation
class DeleteArticleViewModel{
    
    func deleteArticle(id: Int, completionHandler: @escaping (CompletionHandler<String>) -> Void){
        
        DataAccess.shared.deleteArticle(url: API.ARTICLE_DELETE(id: id)) { (response) in
            switch response{
            case .success(let message):
                completionHandler(.success(message))
            case .failure(let message):
                completionHandler(.failure(message))
            }
        }
    }
}
